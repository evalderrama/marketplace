import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './Pages/App/App';
import reportWebVitals from './reportWebVitals';
import Profile from './Pages/Profile/Profile';
import History from './Pages/History/History';
import Password from './Pages/Password/Password';
import Buy from './Pages/Buy/Buy';
import Returns from './Pages/Returns/Returns';
import { BrowserRouter, Routes, Route } from "react-router-dom";

export default function RoutesApp() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<App />} />
          <Route path="*" element={<App />} />
        </Route>
        <Route path="home" element={<App />} />
        <Route path="profile" element={<Profile />} />
        <Route path="history" element={<History />} />
        <Route path="change-password" element={<Password />} />
        <Route path="buy" element={<Buy />} />
        <Route path="returns" element={<Returns />} />
      </Routes>
    </BrowserRouter>
  );
}


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RoutesApp />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
