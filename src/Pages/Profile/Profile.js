import './Profile.css';
import ProfileForm from '../../Components/ProfileForm';
import Navbar from '../../Components/Navbar';
function Profile() {
  
  return (
    <div class="row"> 
      <div class="col-md">
        <div class="row">
          <div class="col-md">
            <Navbar Username='Erick'></Navbar>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-12 text-center">
                  <h5>Mi perfil</h5>
              </div>
              <ProfileForm></ProfileForm>
            </div>
          </div>
          <div class="col-md-3">
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;
