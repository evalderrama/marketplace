import './Password.css';
import Navbar from '../../Components/Navbar';
import ChangePasswordForm from '../../Components/ChangePassword';
function Password() {
  
  return (
    <div class="row"> 
      <div class="col-md">
        <div class="row">
          <div class="col-md">
            <Navbar Username='Erick'></Navbar>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-12 text-center">
                  <h5>Nueva contraseña</h5>
              </div>
              <ChangePasswordForm></ChangePasswordForm>
            </div>
          </div>
          <div class="col-md-3">
          </div>
        </div>
      </div>
    </div>
  );
}

export default Password;
