import './History.css';
import CardHistory from '../../Components/CardHistory';
import Navbar from '../../Components/Navbar';
function History() {
  const historyItems = [
    {
      name: 'Producto A',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.. ',
      price: 320.45,
      date:'25/12/2023'
    },
    {
      name: 'Producto B',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.. ',
      price: 56.22,
      date:'25/12/2023'
    },
    {
      name: 'Producto C',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.. ',
      price: 200,
      date:'25/12/2023'
    }
  ];
  return (
    <div class="row">
      <div class="col-md">
        <div class="row">
          <div class="col-md">
            <Navbar Username='Erick'></Navbar>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
          </div>
          <div class="col-md-6">
              <div class="row">
              <div class="col-md-12 text-center">
                  <h5>Mis Compras</h5>
              </div>
                {
                  historyItems.map(
                    p => (
                      <div class="col-md-6">
                        <CardHistory name={p.name} description={p.description} price={p.price} date={p.date}></CardHistory>
                      </div>
                    )
                  )
                }
              </div>
            </div>
          <div class="col-md-3">
          </div>
        </div>
      </div>
    </div>
  );
}

export default History;
