import './App.css';
import Navbar from '../../Components/Navbar';
import Card from '../../Components/Card';

function App() {

  const products = [
    {
      name: 'Producto A',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.. ',
      price: 320.45
    },
    {
      name: 'Producto B',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.. ',
      price: 56.22
    },
    {
      name: 'Producto C',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.. ',
      price: 200
    },
    {
      name: 'Producto D',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
      price: 5433
    }
  ];
  return (
    <>
      <div class="row">
        <div class="col-md">
          <div class="row">
            <div class="col-md">
              <Navbar Username='Erick'></Navbar>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
              <div class="row">
              <div class="col-md-12 text-center">
                  <h5>Bienvenido</h5>
              </div>
              <div class="col-md-12 mt-2">
                <input class="form-control form-control-lg" type="text" placeholder="Buscar producto..." />
              </div>
                {
                  products.map(
                    p => (
                      <div class="col-md-6">
                        <Card name={p.name} description={p.description} price={p.price}></Card>
                      </div>
                    )
                  )
                }
              </div>
            </div>
            <div class="col-md-3">
            </div>
          </div>
        </div>
      </div>
    </>

  );
}

export default App;
