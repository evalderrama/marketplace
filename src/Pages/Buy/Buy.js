import './Buy.css';
import ClientInformationForm from '../../Components/ClientInformationForm';
import CardBuy from '../../Components/CardBuy';
import Navbar from '../../Components/Navbar';
function Buy() {
  const product= {
    name: 'Producto A',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.. ',
    price: 320.45
  }
  return (
    <div class="row"> 
      <div class="col-md">
        <div class="row">
          <div class="col-md">
            <Navbar Username='Erick'></Navbar>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-12 text-center">
                  <h5>Comprar: Producto A</h5>
              </div>
              <CardBuy name={product.name} description={product.description} price={product.price}></CardBuy>
              <ClientInformationForm></ClientInformationForm>
            </div>
          </div>
          <div class="col-md-3">
          </div>
        </div>
      </div>
    </div>
  );
}

export default Buy;
