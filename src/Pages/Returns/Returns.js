import './Returns.css';
import ReasonReturnForm from '../../Components/ReasonReturnForm';
import CardBuy from '../../Components/CardBuy';
import Navbar from '../../Components/Navbar';
function Returns() {
  const product= {
    name: 'Producto A',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.. ',
    price: 320.45
  }
  return (
    <div class="row"> 
      <div class="col-md">
        <div class="row">
          <div class="col-md">
            <Navbar Username='Erick'></Navbar>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-12 text-center">
                  <h5>Devolver: Producto A</h5>
              </div>
              <CardBuy name={product.name} description={product.description} price={product.price}></CardBuy>
              <ReasonReturnForm></ReasonReturnForm>
            </div>
          </div>
          <div class="col-md-3">
          </div>
        </div>
      </div>
    </div>
  );
}

export default Returns;
