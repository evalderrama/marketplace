import React, { useEffect, useState } from 'react'
import './ItemCounter.css';
function ItemCounter() {
    const [countItem, setCountItem] = useState(0);
    const [message, setMessage] = useState("");

    useEffect(() => {
        if(countItem==0)
            setMessage('Al menos 1.');
        else
            setMessage('');
      })

    return (
        <div class="row item-counter">
            <div class="col-4">
                <button class="btn btn-primary item-counter__less" onClick={() =>  setCountItem(countItem>0?countItem -1:0 )}>-</button>
            </div>
            <div class="col-4 item-counter__count">
                {countItem}
            </div>
            <div class="col-4">
                <button class="btn btn-primary item-counter__plus" onClick={() => setCountItem(countItem +1 )}>+</button>
            </div>
            <small class="item-counter__message">{message}</small>
        </div>
    )
}

export default ItemCounter