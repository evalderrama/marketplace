import React from 'react'
import '../Pages/App/App.css';
function ReasonReturnForm(props) {
    return (
        <div class="row">
            <div class="col-md-12">
                <div class="mb-3">
                    <label for="lastname" class="form-label">Motivo de devolución:</label>
                    <select class="form-control" id="city">
                        <option>-Seleccione-</option>
                        <option value="1">No es la referencia que solicite</option>
                        <option value="2">Llegó dañado</option>
                        <option value="3">Otro</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Descripción:</label>
                    <textarea class="form-control" type="text" id="description">
                    </textarea>
                </div>
            </div>
            <div class="col-md-12">
                <button class="btn btn-danger">
                    <i class="fa fa-save"></i>&nbsp;Devolver :(
                </button>
            </div>
        </div>
    )
}

export default ReasonReturnForm