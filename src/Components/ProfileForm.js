import React from 'react'
import '../Pages/App/App.css';
function ProfileForm(props) {
    return (
        <div class="row">
            <div class="col-md-12">
                <div class="mb-3">
                    <label for="name" class="form-label">Nombres:</label>
                    <input class="form-control" type="text" id="name" />
                </div>
                <div class="mb-3">
                    <label for="lastname" class="form-label">Apellidos:</label>
                    <input class="form-control" type="text" id="lastname" />
                </div>
                <div class="mb-3">
                    <label for="lastname" class="form-label">Email:</label>
                    <input class="form-control" type="text" id="lastname" />
                </div>
            </div>
            <div class="col-md-12">
                <button class="btn btn-primary">
                    <i class="fa fa-save"></i>&nbsp;Guardar cambios
                </button>
            </div>
        </div>
    )
}

export default ProfileForm