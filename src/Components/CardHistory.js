import React from 'react'
import '../Pages/App/App.css';
import { Link } from "react-router-dom";
function CardHistory(props) {
    return (
        <div class="card card__market">
            <img src="https://picsum.photos/seed/picsum/200/300" class="card__card_market__imgtop" alt="..." />
            <div class="card-body">
                <h5 class="card-title">{props.name}</h5>
                <p class="card-text">
                    {props.description}
                </p>
                <div class="row">
                    <div class="col-6">
                        <h6 class="card-title">Precio:${props.price}</h6>
                    </div>
                    <div class="col-6">
                        <h6 class="card-title">Fecha: {props.date}</h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <Link to="/returns" class="btn btn-danger">
                          <i class="fa fa-rotate-left"></i>&nbsp;Devolver
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardHistory