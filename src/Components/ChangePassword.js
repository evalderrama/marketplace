import React from 'react'
import '../Pages/App/App.css';
function ChangePasswordForm(props) {
    return (
        <div class="row">
            <div class="col-md-12">
                <div class="mb-3">
                    <label for="password" class="form-label">Contraseña:</label>
                    <input class="form-control" type="password" id="password" />
                </div>
                <div class="mb-3">
                    <label for="password-repeat" class="form-label">Confirme contraseña:</label>
                    <input class="form-control" type="password" id="password-repeat" />
                </div>
            </div>
            <div class="col-md-12">
                <button class="btn btn-primary">
                    <i class="fa fa-save"></i>&nbsp;Guardar nueva contraseña
                </button>
            </div>
        </div>
    )
}

export default ChangePasswordForm