import React, { useState } from 'react'
import { Outlet, Link } from "react-router-dom";
import Username from './Username'
function Navbar(props){
    const [loggedUser, setLoggedUser] = useState(true);
    return ( 
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Marketplace</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                <Link class="nav-link" to="/home">
                    <i class="fa fa-home"></i>&nbsp;
                    Tienda
                </Link>
                </li>
                <li class="nav-item">
                    <Link class="nav-link" to="/history">
                        <i class="fa fa-shopping-bag"></i>&nbsp;
                        Mis Compras
                    </Link>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <Username loggedUser="true" Username="Erick"></Username>
                </a>
                <ul class="dropdown-menu">
                    <li><Link class="dropdown-item" to="/profile">Mi perfil</Link></li>
                    <li><Link class="dropdown-item" to="/change-password">Cambiar contraseña</Link></li>
                    <li><a class="dropdown-item" href="#">Cerrar sesión</a></li>
                </ul>
                </li>
            </ul>
            </div>
        </div>
        </nav>
    )
}

export default Navbar