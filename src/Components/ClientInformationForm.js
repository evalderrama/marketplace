import React from 'react'
import '../Pages/App/App.css';
function ClientInformationForm(props) {
    return (
        <div class="row">
            <div class="col-md-12">
                <div class="mb-3">
                    <label for="name" class="form-label">Dirección de envio:</label>
                    <input class="form-control" type="text" id="name" />
                </div>
                <div class="mb-3">
                    <label for="lastname" class="form-label">País:</label>
                    <select class="form-control" id="country">
                        <option>-Seleccione-</option>
                        <option value="1">Colombia</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="lastname" class="form-label">Estado:</label>
                    <select class="form-control" id="state">
                        <option>-Seleccione-</option>
                        <option value="1">Magdalena</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="lastname" class="form-label">Ciudad:</label>
                    <select class="form-control" id="city">
                        <option>-Seleccione-</option>
                        <option value="1">Santa Marta</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="receivePerson" class="form-label">Nombre Persona Recibe:</label>
                    <input class="form-control" type="text" id="receivePerson" />
                </div>
            </div>
            <div class="col-md-12">
                <button class="btn btn-primary">
                    <i class="fa fa-save"></i>&nbsp;Pagar
                </button>
            </div>
        </div>
    )
}

export default ClientInformationForm